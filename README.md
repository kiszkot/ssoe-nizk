# Simulation-Sound Online-Extractable Non-Interactive Key-Exchange

This is the repository for a Simulation-Sound Online-Extractable NIZK
to use with the SWOOSH algorithm in order to achieve active security.

This code is based on the following paper from 2014:

1. Unruh, D. (2015). Non-Interactive Zero-Knowledge Proofs in the
   Quantum Random Oracle Model. In: Oswald, E., Fischlin, M. (eds)
   Advances in Cryptology - EUROCRYPT 2015. EUROCRYPT 2015.
   Lecture Notes in Computer Science(), vol 9057. Springer, Berlin, Heidelberg.
   [https://doi.org/10.1007/978-3-662-46803-6_25]
