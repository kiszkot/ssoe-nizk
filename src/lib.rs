use rand::Rng;
use sha3::{
    digest::{ExtendableOutput, Update, XofReader},
    Shake128,
};

pub const SYMBYTES: usize = 32;

pub struct Pi {
    com: Vec<u8>,
    ch: Vec<Vec<u8>>,
    h: Vec<Vec<u8>>,
    resp: Vec<u8>,
}

pub fn p_oe(x: [u8; SYMBYTES], w: [u8; SYMBYTES]) -> Pi {
    let mut rng = rand::rng();

    // t * m proofs
    // m = 2^k, 2 <= m <= | N_ch |
    // Arbitrary value
    let m: u8 = (1 << 5) - 1;
    let t: u8 = rng.random::<u8>();

    let mut com: Vec<u8> = vec![0; t as usize];
    let mut ch: Vec<Vec<u8>> = vec![vec![0; m as usize]; t as usize];
    let mut resp: Vec<Vec<u8>> = vec![vec![0; m as usize]; t as usize];
    let mut h: Vec<Vec<u8>> = vec![vec![0; m as usize]; t as usize];

    // Create proofs
    for i in 1..=t as usize {
        com[i] = p_1_sigma(x, w);

        for j in 1..=m as usize {
            let mut tmp = rng.random::<u8>();
            while !ch[i].contains(&tmp) {
                tmp = rng.random::<u8>();
            }
            ch[i][j] = tmp;

            resp[i][j] = p_2_sigma(ch[i][j]);
        }
    }

    // Commit to responses
    for i in 1..=t as usize {
        for j in 1..=m as usize {
            h[i][j] = g_oracle(resp[i][j]);
        }
    }

    // Get challenges by hashing
    let mut challenge = h_oracle(x, &com, &ch, &h);

    Pi {
        com: com,
        ch: ch,
        h: h,
        resp: resp
            .into_iter()
            .enumerate()
            .map(|a| a.1[challenge[a.0] as usize])
            .collect(),
    }
}

// polymomial-time prover 1
fn p_1_sigma(x: [u8; SYMBYTES], w: [u8; SYMBYTES]) -> u8 {
    // TODO
    0
}

// polymomial-time prover 2
fn p_2_sigma(ch: u8) -> u8 {
    // TODO
    0
}

// p = q = 8 -- number of bits in input and output
// r = 4 -- block size
// t = ceil(p/r) = ceil(8/4) = 2 -- number of r-bit
// blocks that represent a key
// T[t][2^r] -- table of random numbers
fn g_oracle_init_old() -> [[u8; 16]; 2] {
    let mut rng = rand::rng();

    let mut t_table: [[u8; 16]; 2] = [[0; 16]; 2];
    for i in 0..2 {
        for j in 0..16 {
            t_table[i][j] = rng.random::<u8>();
        }
    }
    t_table
}

// Random oracle G
// This oracle is generated using tabulation hashing
// TODO: Check if this is enough
fn g_oracle_old(resp: u8, t_table: [[u8; 16]; 2]) -> u8 {
    // TODO: Decide if table initialization needs to happen
    // after every use, or once every few uses, or once
    // for program run
    let mut res: u8 = 0;
    for i in 0..2 {
        res ^= t_table[i][(resp >> 2 * i) as usize];
    }
    res
}

// Random oracle G
// > If a scheme is secure in the random oracle model, then
// > it is secure if we instantiate it with a particular hash
// > function like SHAKE128 as long as the hash function isn't
// > too badly broken.
// We therefore use SHAKE128
fn g_oracle(resp: u8) -> u8 {
    let mut hasher = Shake128::default();
    hasher.update(&[resp]);
    let mut reader = hasher.finalize_xof();
    let mut result = [0u8; 1];
    reader.read(&mut result);
    result[0]
}

// Random oracle H
fn h_oracle(x: [u8; SYMBYTES], com: &Vec<u8>, ch: &Vec<Vec<u8>>, h: &Vec<Vec<u8>>) -> Vec<u8> {
    // TODO
    // Hash everything
    vec![0; 12]
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_g_oracle() {
        let left = g_oracle(12u8);
        let right = g_oracle(12u8);
        assert_eq!(left, right);
        let different_right = g_oracle(13u8);
        assert_ne!(left, different_right);
    }

    #[test]
    fn test_g_oracle_old() {
        let t_table = g_oracle_init_old();
        let left = g_oracle_old(12, t_table);
        let right = g_oracle_old(12, t_table);
        assert_eq!(left, right);
    }
}
