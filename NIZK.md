# Trying to make sense of the pseudocode

## Poe

```
Input: (x,w) in R

// Create t * m proofs (com_i, ch_i,j, resp_i,j)
for i = 1 to t do
    com_i <- P^1_Sigma (x,w)
    for j = 1 to m do
        ch_i,j <- N_ch \ {ch_i,1 ... ch_i,j-1}
        resp_i,j <- P^2_Sigma (ch_i,j)

// Commit to responses
for i = 1 to t do
    for j = 1 to m do
        h_i,j := G(resp_i,j)

// Get challenges by hasing
J_1 || ... || J_t := H(x, (com_i), (ch_i,j)_i,j, (h_i,j)_i,j)

// Return proof (only some responses)
return pi := ((com_i)_i, (ch_i,j)_i,j, (h_i,j)_i,j, (resp_i,J_i)_i)
```

## Voe

```
Input: (x,pi) with
    pi = ((com_i)_i, (ch_i,j)_i,j, (h_i,j)_i,j, (resp_i,J_i)_i)
J_1 || ... || J_t := H(x, (com_i), (ch_i,j)_i,j, (h_i,j)_i,j)

for i = 1 to t do
    check ch_i,1, ..., ch_i,m pairwise distinct
for i = 1 to t do
    check V_Sigma(x, com_i, ch_i,J_i, resp_i) = 1
for i = 1 to t do
    check H_i,J_i = G(resp_i)
if all check passed then
    return 1
```

## What ChatGPT says

```python
import hashlib
import random

# Define random oracle G
def G(message):
    return hashlib.sha256(str(message).encode()).hexdigest()

# Define random oracle H
def H(*args):
    message = ''.join(args)
    return hashlib.sha256(message.encode()).hexdigest()

# Mock functions P^1_Sigma and P^2_Sigma
def P1_Sigma(x, w):
    # Mock implementation, replace with actual code
    return "mock_com", "mock_ch"

def P2_Sigma(challenge):
    # Mock implementation, replace with actual code
    return "mock_resp"

# Mock function V_Sigma
def V_Sigma(x, com, ch, resp):
    # Mock implementation, replace with actual code
    return random.randint(0, 1)

# Prover function
def prover(x, w, t, m):
    com_list = []
    ch_list = []
    resp_list = []
    h_list = []

    for i in range(t):
        com_i, ch_i = P1_Sigma(x, w)
        for j in range(m):
            ch_i_j = random.randint(0, 1000)  # Random challenge
            ch_list.append(ch_i_j)
            com_i = H(com_i, str(ch_i_j))
            resp_i_j = P2_Sigma(ch_i_j)
            resp_list.append(resp_i_j)
            h_i_j = G(resp_i_j)
            h_list.append(h_i_j)

        com_list.append(com_i)

    challenges_concatenated = ''.join(str(ch) for ch in ch_list)
    J_concatenated = H(x, ''.join(com_list), challenges_concatenated, ''.join(h_list))

    # Only some responses are returned
    resp_to_return = [resp_list[J_concatenated[i]] for i in range(t)]

    return com_list, ch_list, h_list, resp_to_return

# Verifier function
def verifier(x, pi):
    com_list, ch_list, h_list, resp_list = pi

    challenges_concatenated = ''.join(str(ch) for ch in ch_list)
    J_concatenated = H(x, ''.join(com_list), challenges_concatenated, ''.join(h_list))

    # Check pairwise distinctness of challenges
    if len(set(ch_list)) != len(ch_list):
        return 0

    # Check responses with verifier
    for i in range(len(com_list)):
        if V_Sigma(x, com_list[i], ch_list[i], resp_list[i]) != 1:
            return 0

    # Check H_i,J_i = G(resp_i)
    for i in range(len(h_list)):
        if H(ch_list[J_concatenated[i]], resp_list[i]) != h_list[i]:
            return 0

    # All checks passed
    return 1

# Example usage
x = "example_x"
w = "example_w"
t = 3
m = 2

pi = prover(x, w, t, m)
print("Proof:", pi)
print("Verification result:", verifier(x, pi))
```
